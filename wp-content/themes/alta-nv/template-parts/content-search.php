<?php
/**
 * Template part for displaying results in search pages
 */
?>

<article <?php post_class( 'entry' ); ?>>
	<header class="entry__header">
		<?php the_title( sprintf( '<h2 class="entry__title"><a href="%s"', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	</header><!-- .entry-header -->

	<div class="entry__summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-content -->
</article><!-- .entry -->
