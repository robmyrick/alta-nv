<?php get_header(); ?>

<?php

  $sidebar_image = get_field('featured_image');
  $sidebar_large_headline = get_field('sidebar_large_headline');
  $sidebar_small_headline = get_field('sidebar_small_headline');
  $sidebar_button_text = get_field('sidebar_button_text'); 
  $sidebar_button_link = get_field('sidebar_button_link'); ?>
  
  <div id="header-sidebar" class="cell medium-3 large-3">
    <div class="sidebar-fixed">
      <img src="<?= (!empty($sidebar_image) ? $sidebar_image['url'] : ''); ?>" alt="Alta NV" />
      <div class="textbox-sidebar">
        <h3><?= $sidebar_large_headline; ?></h3>
        <p><?= $sidebar_small_headline; ?></p>
      </div> <!-- .textbox -->
  
      <div class="cta-sidebar-anchor"></div>
  
      <div id="cta-sidebar">
        <a href="<?= $sidebar_button_link; ?>" class="cta-button"><?= $sidebar_button_text; ?></a>
      </div> <!-- .cta-sidebar -->
  
      <!--<span class="heroTab">Text Us</span>-->
    </div> <!-- .sidebar-fixed -->
  </div> <!-- .cell -->

<?php get_template_part( 'template-parts/content', 'page' ); ?>

<?php get_footer(); ?>

