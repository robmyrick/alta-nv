<?php
/**
 * Filter Yoast SEO Metabox Priority
 */

function filter_yoast_seo_metabox() {
	return 'low';
}

add_filter( 'wpseo_metabox_prio', 'filter_yoast_seo_metabox' );
