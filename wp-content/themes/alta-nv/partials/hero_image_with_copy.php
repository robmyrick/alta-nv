<?php

  $hero = get_sub_field('hero_image'); 
  $bg_color = get_sub_field('bg_color'); 
  $copy = get_sub_field('copy'); 
  $switch_columns = get_sub_field('switch_columns'); 
  $expanded_grid = get_sub_field('expanded_grid'); 
  $show_scroll = get_sub_field('scroll_indicator'); ?>

<section id="section<?= get_row_index(); ?>" class="section hero-image-with-copy" style="<?= (!empty($bg_color) ? 'background-color:'.$bg_color : ''); ?>">
  <div class="grid-container <?= ($expanded_grid == true && $switch_columns == false ? 'grid-expanded' : 'full'); ?>">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-5 copy <?= ($switch_columns == true ? 'reversed' : ''); ?>">
        <div id="copy">
          <?= $copy; ?>
          <? if ($show_scroll): ?>
            <div id="scroll">
              <a href="#">Scroll</a>
            </div> <!-- #scroll -->
          <?php endif; ?>
        </div> <!-- #copy -->
      </div> <!-- .cell -->

      <div class="cell small-12 medium-12 large-7 hero">
        <div id="hero">
          <img src="<?= (!empty($hero) ? $hero['url'] : ''); ?>" alt="<?= $hero['alt']; ?>" />
        </div> <!-- #hero -->
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->