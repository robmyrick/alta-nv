<section id="section<?= get_row_index(); ?>" class="section contact-section grid-container full">
  <div class="grid-x">
    <div class="cell large-6">
    <div id="contact-form">
        <div class="contact-form__title">
        <h2>Get In Touch</h2>
        <div id="social">
            <a class="facebook" href="https://facebook.com" target="_blank" style="visibility:hidden"></a>
            <a class="instagram" href="https://instagram.com" target="_blank" style="visibility:hidden"></a>
        </div> <!-- #social -->
        </div> <!-- .contact-form__title -->
        <p>What will you discover while living a lifestyle worthy of NV? Please submit your information below to receive more information about living at Alta NV.</p>
        <?= do_shortcode('[gravityform id=1 title=false description=false ajax=true]'); ?>
    </div> <!-- #contact-form -->
    </div> <!-- .cell -->

    <div class="cell large-6">
      <div id="map"></div>
    </div>
  </div> <!-- .grid-x -->
</section>