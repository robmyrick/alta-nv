<?php 

  $i = 1;
  $started = false;
  $args = array(
    'post_type' => 'floor_plans',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'order' => 'ASC'
  );

  $floor_plans_loop = new WP_Query( $args );

  if ($floor_plans_loop->have_posts()): ?>
    <section id="section<?= get_row_index(); ?>" class="section floor_plans">
        <div class="grid-container">
          <div class="grid-x">
            <div class="cell small-12 medium-12 large-12 floor-plan-tabs">
              <ul id="floor-tabs" class="tab-menu"></ul>
            </div> <!-- .cell -->

            <div class="cell small-12 medium-12 large-12 floor-plan-listings">
              <div class="grid-container floor-plan-wrapper"></div>
            </div> <!-- .cell -->
          </div> <!-- .grid-x -->
        </div> <!-- .grid-container -->
      </section>
      <?php
      wp_reset_postdata();
  endif; ?>