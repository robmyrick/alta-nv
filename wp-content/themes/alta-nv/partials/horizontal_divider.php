<?php $expanded_grid = get_sub_field('expanded_grid'); ?>

<section id="section<?= get_row_index(); ?>" class="section horizontal-divider">
  <div class="grid-container <?= ($expanded_grid == true && $switch_columns == false ? 'grid-expanded' : ''); ?>">
    <div class="grid-x">
      <div class="cell small-12 medium-12 large-12 alta-divider">
        <hr/>
      </div> <!-- .cell -->
    </div> <!-- .grid-x -->
  </div> <!-- .grid-container -->
</section> <!-- section -->